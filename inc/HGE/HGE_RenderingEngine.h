#ifndef HGE_RENDERING_ENGINE_H
#define HGE_RENDERING_ENGINE_H

#include "HGE_Texture.h"
#include "HGE_Shader.h"

typedef struct {
  bool isMainCamera;
  bool isOrthographic;

  float fov;
  float width;
  float height;
  float aspect;
  float zNear;
  float zFar;
} hge_camera;

void hgeClearColor(float r, float g, float b, float a);
void hgeClear(long int mask);

void hgeRenderInit();
void hgeRenderSprite(hge_shader shader, hge_texture sprite, hge_vec3 position, hge_vec3 scale, float rotation);
void hgeRenderSpriteSheet(hge_shader shader, hge_texture sprite, hge_vec3 position, hge_vec3 scale, float rotation, hge_vec2 frame_resolution, hge_vec2 frame);
void hgeRenderQuad();

void hgeRenderFrame();

#endif
