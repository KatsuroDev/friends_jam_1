#include <HGE/HGE_Core.h>
#include "utility/assets_macros.h"
#include "utility/utility.h"
#include "components/player/player.h"
#include "components/level/level.h"
#include <stdio.h>

int main(int argc, char *argv[])
{
    printf("Lol pony, alexis is pony. love pony. play pony game w/ me!\n");
    bool is_host = false;
    if(argc > 1)
    {
        if(strcmp(argv[1], "host") == 0)
            is_host = true;
    }

    hge_window window = {is_host ? "Anima Mundi - Server" : "Anima Mundi", 800, 600};
    hgeInit(60, window, HGE_INIT_ALL);
    if(is_host) {
        int port;
        int max_connection;
        char* start[5];
        printf("Enter Server's Port : ");
        scanf("%d", &port);
        printf("Enter Server's maximum player : ");
        scanf("%d", &max_connection);
        printf("Write 'start' to start the server\n>");
        scanf("%s", start);
        if(strcmp(start, "start") == 0)
            hgeNetworkHost(port, max_connection);
        else
            return 0;
    }
    else
    {
        char* server_ip[100];
        int port;
        printf("Enter Server's IP : ");
        scanf("%s", server_ip);
        printf("Enter Server's Port : ", port);
        scanf("%d", &port);
        hgeNetworkConnect(server_ip, port);
        hgeNetworkSendPacket("Hello, World!\0", 14);
    }

    addAllSystems();
    loadTextures();

    if(is_host)
    {
        addCamera();
        addNetworkManager();
    }
    else
        addCamera();

    createLevel1();

    if(!is_host)
        createPlayer(-4*TILE, 10*TILE, true);

    hgeStart();
    return 0;
}
