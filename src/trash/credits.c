/*#include "credits.h"

void playFirstCredits()
{
    hge_entity* ug_thumbnail = hgeCreateEntity();
    hge_transform transform;
    transform.position.x = 0;
    transform.position.y = 0;
    transform.position.z = 0;
    transform.scale.x = 2;
    transform.scale.y = 1.8f;
    transform.scale.z = 0;
    credits_component data;
    data.alpha = 0.0f;
    data.time_at_screen = 3000.0f;
    data.timer = 0.0f;
    data.functionToCallWhenKilled = playTitle;
    data.texture = hgeLoadTexture("res/credits/Usergames_Thumbnail.png");
    data.shader = hgeLoadShader("res/shaders/credits.vs", NULL, "res/shaders/credits.fs");
    hgeAddComponent(ug_thumbnail, hgeCreateComponent("Transform", &transform, sizeof(transform)));
    hgeAddComponent(ug_thumbnail, hgeCreateComponent("Credit", &data, sizeof(data)));
}

void CreditSystem(hge_entity* e, hge_transform* transform, credits_component* data)
{
    hgeClearColor(0.0235f, 0.3137f, 0.5804f, 0.0f);
    hgeClear(16384);
    data->timer += hgeDeltaTime();
    if(data->timer >= data->time_at_screen)
    {
        data->functionToCallWhenKilled();
        hgeDestroyEntity(e);
    }
    data->alpha = sin(hgeDeltaTime());
    renderCredit(data->alpha, data->texture, data->shader, transform);
}

void renderCredit(float alpha, hge_texture texture, hge_shader shader, hge_transform* transform)
{
    hgeUseShader(shader);
    hge_mat4 model = hgeMathMat4(1.0f);
    model = hgeMathMat4Translate(model, transform->position);
    model = hgeMathMat4Scale(model, transform->scale);
    hgeShaderSetMatrix4(shader, "model", model);
    hgeShaderSetFloat(shader, "alpha", alpha);

    hgeBindTexture(texture);
    hgeRenderQuad();
}

void playTitle() {}
void playHGE() {}
void playGame() {}
*/
