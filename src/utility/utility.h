#ifndef GT_FUNCTIONS_H
#define GT_FUNCTIONS_H

#include <HGE/HGE_Core.h>

void loadTextures();

void addAllSystems();

void addCamera();

void addNetworkManager();

void NetworkingSystem(hge_entity* e, tag_component tag);



#endif
