#include "utility.h"
#include "../components/player/player.h"
#include "../components/character/character.h"
#include "../components/level/level.h"


void loadTextures()
{
    hgeResourcesLoadTexture("res/asset/colored_packed.png", "asset_pack");
    hgeResourcesLoadTexture("res/asset/colored_transparent_packed.png", "asset_pack_transparent");
    hgeResourcesLoadTexture("res/levels/level1.png", "Level_1_Normal");
}

void addAllSystems()
{
    hgeAddBaseSystems();
    hgeAddSystem(PlayableSystem, 2, "Playable", "Character");
    hgeAddSystem(PlayerSystem, 2, "Transform", "Player");
    hgeAddSystem(CharacterSystem, 2, "Transform", "Character");
    hgeAddSystem(LevelSystem, 2, "Transform", "Level");
    hgeAddSystem(NetworkingSystem, 1, "NetworkManager");
}

void addCamera()
{
    hge_entity* camera_entity = hgeCreateEntity();
    hge_camera cam = {true, true, 1.f/6.f, hgeWindowWidth(), hgeWindowHeight(), (float)hgeWindowWidth()/(float)hgeWindowHeight(), -100.f, 100.f};
    hge_vec3 camera_position = {0, 0, 2};
    orientation_component camera_orientation = {0.f, -90.0f, 0.f};
    hgeAddComponent(camera_entity, hgeCreateComponent("Camera", &cam, sizeof(cam)));
    hgeAddComponent(camera_entity, hgeCreateComponent("Position", &camera_position, sizeof(camera_position)));
    hgeAddComponent(camera_entity, hgeCreateComponent("Orientation", &camera_orientation, sizeof(camera_orientation)));
    tag_component activecam_tag;
    hgeAddComponent(camera_entity, hgeCreateComponent("ActiveCamera", &activecam_tag, sizeof(activecam_tag)));
}

void addNetworkManager()
{
    hge_entity* e = hgeCreateEntity();
    tag_component networking_tag;
    hgeAddComponent(e, hgeCreateComponent("NetworkManager", &networking_tag, sizeof(networking_tag)));
}

void NetworkingSystem(hge_entity* e, tag_component tag)
{
    hge_network_events* eventlist = hgeNetworkEventList();
    for(int i = 0; i < eventlist->num_messages; i++) {
        printf("%s\n", eventlist->messages[i].data);
        free(eventlist->messages[i].data);
    }
    eventlist->num_messages = 0;
}
