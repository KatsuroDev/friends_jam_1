#include "level.h"
#include <HGE/HGE_Core.h>
#include "../utility/assets_macros.h"

void LevelSystem(hge_entity* e, hge_transform* transform, level_component* data)
{
    //printf("%f\n", sizeof(data->walls) / sizeof(data->walls[0]));

    hgeRenderSprite(hgeResourcesQueryShader("sprite_shader"), data->normal_sprite, transform->position, transform->scale, 0);
}

void createLevel(float scale_x, float scale_y, hge_texture normal_sprite, hge_texture anima_mundi_sprite, hge_transform walls[], int n_wall)
{
    hge_transform transform;
    transform.scale.x = scale_x;
    transform.scale.y = scale_y;
    transform.scale.z = 0;
    transform.position.x = 4;
    transform.position.y = 4;
    transform.position.z = -10;
    level_component data;
    data.normal_sprite = normal_sprite;
    data.anima_mundi_sprite = anima_mundi_sprite;
    data.n_wall = n_wall;
    memcpy(&data.walls[0], &walls[0], sizeof(hge_transform)*n_wall);
    hge_entity* e = hgeCreateEntity();
    hgeAddComponent(e, hgeCreateComponent("Transform", &transform, sizeof(transform)));
    hgeAddComponent(e, hgeCreateComponent("Level", &data, sizeof(data)));
}

void createLevel1()
{
    hge_transform walls[NUM_WALLS_1_N];
    //
    walls[0].position.x = -40;
    walls[0].position.y = 96;
    walls[0].scale.x = 7*TILE;
    walls[0].scale.y = TILE;
    //
    walls[1].position.x = -16;
    walls[1].position.y = 28;
    walls[1].scale.x = TILE;
    walls[1].scale.y = 18*TILE;
    //
    walls[2].position.x = 20;
    walls[2].position.y = -40;
    walls[2].scale.x = 10*TILE;
    walls[2].scale.y = TILE;
    //
    walls[3].position.x = 56;
    walls[3].position.y = -64;
    walls[3].scale.x = TILE;
    walls[3].scale.y = 7*TILE;
    //
    walls[4].position.x = -6;
    walls[4].position.y = -88;
    walls[4].scale.x = 16*TILE;
    walls[4].scale.y = TILE;
    //
    walls[5].position.x = -64;
    walls[5].position.y = 4;
    walls[5].scale.x = TILE;
    walls[5].scale.y = 24*TILE;
    //

    createLevel(18*TILE, 24*TILE, hgeResourcesQueryTexture("Level_1_Normal"), hgeResourcesQueryTexture("Level_1_Normal"), walls, NUM_WALLS_1_N);
}
