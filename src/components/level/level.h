#ifndef GT_LEVEL_H
#define GT_LEVEL_H

#include <HGE/HGE_Core.h>

typedef struct {
    hge_texture normal_sprite;
    hge_texture anima_mundi_sprite;
    hge_transform walls[100];
    int n_wall;
} level_component;

void LevelSystem(hge_entity* e, hge_transform* transform, level_component* data);

void createLevel(float scale_x, float scale_y, hge_texture normal_sprite, hge_texture anima_mundi_sprite, hge_transform walls[], int n_wall);


#endif
