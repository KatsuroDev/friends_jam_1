#include "player.h"
#include "../../utility/assets_macros.h"
#include <stdio.h>

void createPlayer(float x, float y, bool isPlayable)
{

    // CHARACTER
    hge_entity* e = hgeCreateEntity();
    character_component character_data;
    character_data.state = IDLE;
    character_data.movement_direction.x = 0;
    character_data.movement_direction.y = 0;
    character_data.movement_vector.x = 0;
    character_data.movement_vector.y = 0;
    character_data.walk_speed = 25.f;
    character_data.sprint_speed = 40.f;
    character_data.isRunning = false;
    character_data.hasJustCollided = false;
    hgeAddComponent(e, hgeCreateComponent("Character", &character_data, sizeof(character_data)));

    // TRANSFORM
    hge_transform transform;
    transform.position.x = x;
    transform.position.y = y;
    transform.position.z = isPlayable ? 2 : 1;
    transform.scale.x = TILE;
    transform.scale.y = TILE;
    hge_component transform_component = hgeCreateComponent("Transform", &transform, sizeof(transform));
    hgeAddComponent(e, transform_component);

    // FOLLOW
    follow_component follow;
    follow.lock_x = false;
    follow.lock_y = false;
    follow.lock_z = true;
    follow.speed = 10.f;

    hge_transform* follow_transform = e->components[hgeQuery(e, "Transform")].data;
    hge_vec3* player_position = &follow_transform->position;
    follow.target_pos = player_position;

    hge_entity* cam = hgeQueryEntity(1, "ActiveCamera");
    hgeAddComponent(cam, hgeCreateComponent("Follow", &follow, sizeof(follow)));

    // PLAYER
    player_component player_data;
    player_data.sprite_res.x = 16;
    player_data.sprite_res.y = 16;
    player_data.sprite_frame.x = 25;
    player_data.sprite_frame.y = 0;
    hgeAddComponent(e, hgeCreateComponent("Player", &player_data, sizeof(player_data)));
    tag_component playable_tag;
    if(isPlayable)
        hgeAddComponent(e, hgeCreateComponent("Playable", &playable_tag, sizeof(playable_tag)));
    spritesheet_component sprite;
    sprite.flipped = false;
    sprite.FPS = 0;
    sprite.frame = player_data.sprite_frame;
    sprite.num_frames = 0;
    sprite.playing = false;
    sprite.resolution = player_data.sprite_res;
    sprite.spritesheet = hgeResourcesQueryTexture("asset_pack_transparent");
    hgeAddComponent(e, hgeCreateComponent("SpriteSheet", &sprite, sizeof(sprite)));

}


void PlayableSystem(hge_entity* e, tag_component* tag, character_component* character_data)
{

    hge_vec2 direction_vector = {0, 0};

    bool RIGHT = hgeInputGetKey(HGE_KEY_D);
    bool LEFT = hgeInputGetKey(HGE_KEY_A);
    bool UP = hgeInputGetKey(HGE_KEY_W);
    bool DOWN = hgeInputGetKey(HGE_KEY_S);


    if(LEFT || RIGHT || UP || DOWN)
        character_data->state = MOVING;
    if(!(LEFT || RIGHT || UP || DOWN))
        character_data->state = IDLE;

    if(hgeInputGetKey(HGE_KEY_LEFT_SHIFT))
        character_data->isRunning = true;
    else
        character_data->isRunning = false;


    switch(character_data->state)
    {
        case IDLE:
            direction_vector.x = 0;
            direction_vector.y = 0;
            break;
        case MOVING:
            if(LEFT)
                direction_vector.x = -1;
            if(RIGHT)
                direction_vector.x = 1;
            if(UP)
                direction_vector.y = 1;
            if(DOWN)
                direction_vector.y = -1;

            direction_vector = hgeMathVec2Normalize(direction_vector);
            character_data->movement_direction = direction_vector;
            break;
    }
}

void PlayerSystem(hge_entity* e, hge_transform* transform, player_component* player_data)
{

}
