#include "character.h"
#include "../level/level.h"
#include "../../utility/assets_macros.h";

void CharacterSystem(hge_entity* e, hge_transform* transform, character_component* data)
{

    hge_entity* level = hgeQueryEntity(2, "Transform", "Level");
    level_component* level_data = level->components[hgeQuery(level, "Level")].data;
    hge_transform character_transform = *transform;

    bool isColliding = false;

    bool wall0 = (AABB(level_data->walls[0], character_transform));
    bool wall1 = (AABB(level_data->walls[1], character_transform));
    bool wall2 = (AABB(level_data->walls[2], character_transform));
    bool wall3 = (AABB(level_data->walls[3], character_transform));
    bool wall4 = (AABB(level_data->walls[4], character_transform));
    bool wall5 = (AABB(level_data->walls[5], character_transform));

    if(wall0)
        isColliding = true;
    if(wall1)
        isColliding = true;
    if(wall2)
        isColliding = true;
    if(wall3)
        isColliding = true;
    if(wall4)
        isColliding = true;
    if(wall5)
        isColliding = true;

    // Also, keep up the great work!
    // Thank you!
    if(isColliding)
    {

      if(!data->hasJustCollided) {
        data->hasJustCollided = true;
        }
    }
    else
    {
      data->hasJustCollided = false;
    }
    if(data->hasJustCollided)
    {
        if(wall0)
            transform->position.y = level_data->walls[0].position.y - TILE - 0.0001;
        if(wall1)
            transform->position.x = level_data->walls[1].position.x - TILE - 0.0001;
        if(wall2)
            transform->position.y = level_data->walls[2].position.y - TILE - 0.0001;
        if(wall3)
            transform->position.x = level_data->walls[3].position.x - TILE - 0.0001;
        if(wall4)
            transform->position.y = level_data->walls[4].position.y + TILE + 0.0001;
        if(wall5)
            transform->position.x = level_data->walls[5].position.x + TILE + 0.0001;

    }
    if(!data->hasJustCollided)
        CharacterMovement(transform, data);

}


void CharacterMovement(hge_transform* transform, character_component* data)
{
    float acceleration = 10.0f;
    switch (data->state) {
        case IDLE:
            data->movement_vector.x = 0;
            data->movement_vector.y = 0;
            break;
        case MOVING:
            data->movement_vector.x += (data->movement_direction.x*(data->isRunning ? data->sprint_speed : data->walk_speed) - data->movement_vector.x) * acceleration * hgeDeltaTime();
            data->movement_vector.y += (data->movement_direction.y*(data->isRunning ? data->sprint_speed : data->walk_speed) - data->movement_vector.y) * acceleration * hgeDeltaTime();
            break;
    }
    transform->position.x += data->movement_vector.x * hgeDeltaTime();
    transform->position.y += data->movement_vector.y * hgeDeltaTime();
}
