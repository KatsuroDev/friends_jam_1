#ifndef GT_CHARACTER_H
#define GT_CHARACTER_H

#include <HGE/HGE_Core.h>

typedef enum {
    IDLE, MOVING
} character_state;

typedef struct {
    character_state state;
    hge_vec2 movement_direction;
    hge_vec2 movement_vector;
    float walk_speed;
    float sprint_speed;
    bool isRunning;
    bool hasJustCollided;
} character_component;

void CharacterSystem(hge_entity* e, hge_transform* transform, character_component* data);

void CharacterMovement(hge_transform* transform, character_component* data);

#endif
